﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public static PlayerController instance = null;
	public GameObject startPoint;
	public float outerRadius = 0.2f;
	public float innerRadius = 0.1f;
	void Awake ()
	{

		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);    

		DontDestroyOnLoad (gameObject);


	}
	Vector2 getPosition ()
	{

		Vector2 curScreenPoint = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		Vector2 curPosition = Camera.main.ScreenToWorldPoint (curScreenPoint);
		return curPosition;
	}
	void Start () {
		
	}
	void OnMouseUp ()
	{
		#if UNITY_IOS
		// allow unity player to work in iphone mode without using a remote but real ios wont do doulbe calls.
		if (Input.touchCount > 0)
		return;
		#endif
		Vector2 currentPosition = getPosition ();



		OnUp (currentPosition);
	}

	void OnMouseDown ()
	{  
		#if UNITY_IOS
		if (Input.touchCount > 0)
		return;
		#endif
		Vector2 currentPosition = getPosition ();

		OnDown (currentPosition);
	}
	void OnDown(Vector2 position) {
	}

	void OnUp(Vector2 position) {
	
	}
	void OnDrag(Vector2 position) {

	}

	void OnMouseDrag ()
	{
		#if UNITY_IOS
		if (Input.touchCount > 0)
		return;
		#endif


		Vector2 currentPosition = getPosition ();


		OnDrag (currentPosition);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
